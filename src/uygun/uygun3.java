package uygun;

import java.util.Random;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class uygun3 {

	@Test
	public static void main() throws InterruptedException {
		
		WebDriver driver=new FirefoxDriver();
		String parentWindow = driver.getWindowHandle();	
		driver.get("https://www.enuygun.com/otel/");
		
		//Random click popular
		Thread.sleep(2000);
		Random rand = new Random();
		int rand1 = rand.nextInt(23)+ 1;
		   String locator = String.format("//div[2]/div/ul/li["+ rand1 +"]/a"); 
		   driver.findElement(By.xpath(locator)).click();	   
		 
		//Random click hotel
		int hotelCount = driver.findElements(By.xpath("//li/div/div[2]/h2/a")).size();
		Random hotel = new Random();
		int hotel1 = hotel.nextInt(hotelCount-1)+ 1;
			String locator1 = String.format("//li["+ hotel1 +"]/div/div[2]/h2/a"); 
			driver.findElement(By.xpath(locator1)).click();
			Thread.sleep(2000);
		
		//Search Availability	
			Set<String> allWindows = driver.getWindowHandles();
			for(String window:allWindows)
			{
				if(!parentWindow.equalsIgnoreCase(window))
				{
					driver.switchTo().window(window);
				}
			}
			driver.findElement(By.id("room-guest")).click();
			driver.findElement(By.cssSelector("i.fa.fa-plus-circle")).click();
			driver.findElement(By.xpath("//div[2]/button")).click();
			driver.findElement(By.xpath("//form[@id='search-form']/div[2]/div/input")).click();
			outerloop:
			for (int v=2; v<=4; v++){
				for(int y=1; y<=6; y++){
					driver.findElement(By.xpath("//tr["+ v+ "]/td["+ y+"]/div")).click();
					driver.findElement(By.xpath("//tr["+ v+ "]/td["+ (y+1)+"]/div")).click();
					driver.findElement(By.xpath("//div[4]/button")).click();
					Thread.sleep(4000);
					String elm1 = driver.findElement(By.cssSelector("div.similar-hotels-header")).getText();
					if(elm1.contains("Üzgünüz"))
					{
						driver.findElement(By.xpath("//div[2]/div/button")).click();
						driver.findElement(By.xpath("//form[@id='search-form']/div[2]/div/input")).click();
						//Double click problem for dates
						driver.findElement(By.xpath("//form[@id='search-form']/div[2]/div/input")).click();
					}
					else
					{
						driver.findElement(By.xpath("//div[4]/div/span/a")).click();
						System.out.print("Test is success.");
						break outerloop;
					}
				}
			}
			driver.close();
	}
}
