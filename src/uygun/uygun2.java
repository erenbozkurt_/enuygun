package uygun;

import java.util.Random;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class uygun2 {

	@Test
	public static void main() throws InterruptedException {
		
		String[] PlacesArray = {"İstanbul", "İzmir", "Adana", "Trabzon", "Antalya"};
		
		int lent = new Random().nextInt(PlacesArray.length);
		String random = (PlacesArray[lent]);
		PlacesArray = ArrayUtils.remove(PlacesArray, lent);
		String random1 = (PlacesArray[new Random().nextInt(PlacesArray.length)]);
		
		WebDriver driver=new FirefoxDriver();
		
		driver.get("https://www.enuygun.com/ucak-bileti/");
		
		//Select From
		driver.findElement(By.id("from-input")).sendKeys(random);

		if(random == "İstanbul")
		{	
			WebElement DynamicElement = (new WebDriverWait(driver, 2))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[2]/div/span[2]")));
			driver.findElement(By.xpath("//li[2]/div/span[2]")).click();
		}
		else
		{
			WebElement DynamicElement1 = (new WebDriverWait(driver, 2))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li/div/span[2]")));
			driver.findElement(By.xpath("//li/div/span[2]")).click();
		}
		
		//Select To
		driver.findElement(By.id("to-input")).sendKeys(random1);
		
		if (random1 == "İstanbul")
		{
			WebElement myDynamicElement2 = (new WebDriverWait(driver, 2))
					  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[2]/li[2]/div/span[2]")));
			driver.findElement(By.xpath("//ul[2]/li[2]/div/span[2]")).click();
		} 
		else
		{
			WebElement myDynamicElement2 = (new WebDriverWait(driver, 2))
					  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[2]/li/div/span[2]")));
			driver.findElement(By.xpath("//ul[2]/li/div/span[2]")).click();;
		}
		
		//Click Two-Way
		driver.findElement(By.id("oneway-input")).click();
		
		//Click Show non-stop flights only
		driver.findElement(By.id("flight-direct")).click();
		
		//Select Business
		driver.findElement(By.id("select-passenger-btn")).click();
		driver.findElement(By.id("flight-class-select")).sendKeys("Business");
		
		//Save time
		String time1 =driver.findElement(By.id("departure-date-input")).getText();
		String time2 =driver.findElement(By.id("return-date-input")).getText();
		
		//Click Find best deals
		driver.findElement(By.id("submit-icon")).click();
		
		//Verify Text
		String inf = driver.findElement(By.cssSelector("div.info")).getText();		
		if(inf.contains(random) && inf.contains(random1) && inf.contains(time1) && inf.contains(time2))
		{
			System.out.print("true");
		}
		driver.close();
	}
}
