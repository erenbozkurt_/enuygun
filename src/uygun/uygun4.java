package uygun;

import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


public class uygun4 {
	
	@Test
	public void main() throws InterruptedException {
		
		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.enuygun.com/otel/");
		
		//Random click popular
		Thread.sleep(2000);
		Random rand = new Random();
		int rand1 = rand.nextInt(23)+ 1;
		   String locator = String.format("//div[2]/div/ul/li["+ rand1 +"]/a"); 
		   driver.findElement(By.xpath(locator)).click();
		   
		//Select Date And Search
		   driver.findElement(By.xpath("//div[2]/div/input")).click();
		   driver.findElement(By.xpath("//table[2]/tbody/tr[2]/td[3]/div")).click();
		   driver.findElement(By.xpath("//table[2]/tbody/tr[2]/td[4]/div")).click();
		   driver.findElement(By.xpath("//div[4]/button")).click();
		   Thread.sleep(2000);
		   driver.findElement(By.xpath("//div[4]/button")).click();
		   driver.findElement(By.xpath("//div[5]/div/div/div[2]/div[2]/div")).click();
		   driver.findElement(By.xpath("//div[5]/div/div/div[2]/ul/li")).click();
		   Thread.sleep(4000);
		   
		 //Select first hotel
		   driver.findElement(By.cssSelector("a.btn.btn-success.choice-btn")).click();
		   
		 driver.close();
	}
}
